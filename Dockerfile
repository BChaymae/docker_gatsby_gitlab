FROM node as build

WORKDIR /gatsby

 

COPY . .

 

# i: intaller les dependences    g: global

 

RUN npm i -g gatsby-cli

RUN npm i

 

# installer gatsby en global

RUN gatsby build

# Lancer l'application

 

# ENTRYPOINT ["gatsby", "develop", "-H", "0.0.0.0"]

 

# Passer au termibale

# - docker build -t mygatsby .

# - docker run -p 8080:5000 mygatsby

 

 

## Je prend le dossier public et je le mets

FROM nginx as deploy

COPY --from=build /gatsby/public /usr/share/nginx/html

ENTRYPOINT [ "nginx", "-g", "daemon off;" ]